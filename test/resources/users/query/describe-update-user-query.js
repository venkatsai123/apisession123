const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const uuid = require("uuid");
const db = require("db/repository");
const ds = require("helpers/dataSetup");
const RunQuery = require("data/run-query");
const UpdateUserQuery = require("resources/users/query/update-user-query");

describe.only("Update user query", () => {
  let user;
  beforeEach(async () => {
    user = await ds.createSingle(ds.user);
  });

  it("should update user", async () => {
    console.log(user, "user");
    (user.fullName = "venkat"), (user.countryCode = 91);
    console.log(user, "user");
    const updateUserResponse = await db.execute(
      new UpdateUserQuery(user.id, user.fullName, user.countryCode)
    );
    verifyResultOk((updatedUser) => {
       const result = updatedUser[1][0].dataValues
      expect(user.id).eql(result.id);
      expect(user.fullName).eql(result.fullName);
      expect(user.countryCode).eql(result.countryCode);
    })(updateUserResponse);

    let fetchUserResponse = await db.findOne(
      new RunQuery('select * from public."Users" where id=id', { id: user.id })
    );

    console.log("fetchUserResponse", fetchUserResponse);
    verifyResultOk((updatedUser) => {
      expect(user.id).eql(updatedUser.id);
      expect(user.fullName).eql(updatedUser.fullName);
      expect(user.countryCode).eq(updatedUser.countryCode);
    })(fetchUserResponse);
  });

  after(async () => {
    await ds.deleteAll();
  });
});
