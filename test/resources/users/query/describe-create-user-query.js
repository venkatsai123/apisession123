const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const uuid = require("uuid");
const db = require("db/repository");
const ds = require("helpers/dataSetup");
const RunQuery = require("data/run-query");
const CreateUserQuery = require("resources/users/query/create-user-query");

describe("create user query", () => {
  let user;
  beforeEach(async () => {
    user = await ds.buildSingle(ds.user);
  });

  it("should create a user", async () => {
    let createdUserResponse = await db.execute(
      new CreateUserQuery(user.id, user.fullName, user.countryCode)
    );
    verifyResultOk((createdUser) => {
      expect(user.id).to.be.eql(createdUser.id);
      expect(user.fullName).to.be.eql(createdUser.fullName);
      expect(user.countryCode).to.be.eql(createdUser.countryCode);
    })(createdUserResponse);

    let fetchUserResponse = await db.findOne(new RunQuery('select * from public."Users" where id=id',{id:user.id}))
    verifyResultOk((createdUser) => {
        expect(user.id).to.be.eql(createdUser.id);
        expect(user.fullName).to.be.eql(createdUser.fullName);
        expect(user.countryCode).to.be.eql(createdUser.countryCode);
      })(fetchUserResponse);
  });

  after(async () => {
    await ds.deleteAll();
  });
});
