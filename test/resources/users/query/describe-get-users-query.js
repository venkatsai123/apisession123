const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const uuid = require("uuid");
const db = require("db/repository");
const ds = require("helpers/dataSetup");
const GetUserQuery = require("resources/users/query/get-users-query")

describe('get user query', () => {

    let user;   
    beforeEach(async () => {
        user = await ds.createSingle(ds.user);
      });

    it("should get all users", async()=>{
        const fetchUser = await db.find(new GetUserQuery());
        let id,fullName,countryCode;
        fetchUser.value.map((item)=>{
            id = item.dataValues.id
            fullName=item.dataValues.fullName
            countryCode = item.dataValues.countryCode

        })
        expect(id).to.include(user.id)
        expect(fullName).to.include(user.fullName)
        expect(countryCode).to.be.eql(user.countryCode)
    })

    after(async () => {
        await ds.deleteAll();
      });
});