const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const db = require("db/repository");
const CreateUserValidation = require("resources/users/validation/create-user-validation");
const { resolveOk, resolveError } = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const GetUsersQuery = require("resources/users/query/get-users-query");
const uuid = require("uuid");

describe("describe get user", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let userId;
  beforeEach(() => {
    req = {
      body: {},
      params: {},
    };
    userId = uuid.v4();
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  context("Success Senario", () => {
    it("should get users if validations are successful", async () => {
      const returnResult = [
        {
          countryCode: 91,
          fullName: "fullName",
          id: userId,
        },
      ];

      sandbox.mock(db).expects("execute").returns(resolveOk(returnResult));
      const response = await TestRoutes.execute("/users", "Get", req, res);
      expect(response).to.be.eql({
        message: "Fetched users succesfully",
        status: true,
        entity: returnResult,
      });
    });
  });

  it("should not get user if validaitons are failed", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random error"));
    const response = await TestRoutes.executeWithError(
      "/users",
      "Get",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random error", "something Went wrong")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
