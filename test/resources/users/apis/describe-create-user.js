const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const db = require("db/repository");
const CreateUserValidation = require("resources/users/validation/create-user-validation");
const {
  resolveValidationError,
  resolveOk,
  resolveError,
} = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const ValidationError = require("lib/validation-error");
const verifiers = require("helpers/verifiers");
const CreateUserQuery = require("resources/users/query/create-user-query");
const uuid = require("uuid");

describe("describe create user", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let userId;
  beforeEach(() => {
    req = {
      body: {
        fullName: "fullName",
        countryCode: 91,
      },
    };
    userId = uuid.v4();
    sandbox.mock(uuid).expects("v4").returns(userId);
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  context("success Senario", () => {
    it("should create user if valiation is successful", async () => {
      const retrunResult = {
        id: "userId",
        fullName: "sanjeev kumar",
        countryCode: 91,
      };
      sandbox
        .mock(CreateUserValidation)
        .expects("validate")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.eql({
              fullName: req.body.fullName,
              countryCode: req.body.countryCode,
            });
          })
        )
        .returns(resolveOk());
      sandbox
        .mock(db)
        .expects("execute")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.instanceOf(CreateUserQuery);
            expect(args.details).to.be.eql({
              id: userId,
              fullName: req.body.fullName,
              countryCode: req.body.countryCode,
            });
          })
        )
        .returns(resolveOk(retrunResult));

      const response = await TestRoutes.execute("/users", "Post", req, res);
      expect(response).to.be.eql({
        status: true,
        message: "Successfully created user",
        entity: retrunResult,
      });
    });
  });

  it("should not create user when validation is failed", async () => {
    sandbox
      .stub(CreateUserValidation,"validate")
      .returns(resolveValidationError("some error"));
      sandbox.mock(db).expects("execute").never()
    const response = await TestRoutes.executeWithError(
      "/users",
      "Post",
      req,
      res
    );
    expect(response).to.eql(
      new ValidationError(0, "some error")
    );
  });

  // it("should not create user when validation is failed", async () => {
  //   sandbox
  //     .mock(CreateUserValidation)
  //     .expects("validate")
  //     .withArgs(
  //       verifiers.verifyArgs((args) => {
  //         expect(args).to.be.eql({
  //           fullName: req.body.fullName,
  //           countryCode: req.body.countryCode,
  //         });
  //       })
  //     )
  //     .returns(resolveValidationError("CountryCode is mandatory"));
  //   const response = await TestRoutes.executeWithError(
  //     "/users",
  //     "Post",
  //     req,
  //     res
  //   );
  //   expect(response).to.eql(new ValidationError(0, "CountryCode is mandatory"));
  // });

  it("something went wrong unable to create the user", async () => {
    sandbox
      .mock(CreateUserValidation)
      .expects("validate")
      .withArgs(
        verifiers.verifyArgs((args) => {
          expect(args).to.be.eql({
            fullName: req.body.fullName,
            countryCode: req.body.countryCode,
          });
        })
      )
      .returns(resolveOk());
    sandbox
      .mock(db)
      .expects("execute")
      .withArgs(
        verifiers.verifyArgs((args) => {
          expect(args).to.be.instanceOf(CreateUserQuery);
          expect(args.details).to.be.eql({
            id: userId,
            fullName: req.body.fullName,
            countryCode: req.body.countryCode,
          });
        })
      )

      .returns(resolveError("some random Error"));
    const response = await TestRoutes.executeWithError(
      "/users",
      "Post",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random Error", "Failed to create user")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
