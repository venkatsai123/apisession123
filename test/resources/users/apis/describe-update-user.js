const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const db = require("db/repository");
const CreateUserValidation = require("resources/users/validation/create-user-validation");
const UpdateUserValidation = require("resources/users/validation/update-user-validation");
const { resolveOk, resolveError,resolveValidationError } = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const uuid = require("uuid");
const UpdateUserQuery = require("resources/users/query/update-user-query");
const verifiers = require("helpers/verifiers");
const ValidationError = require("lib/validation-error");

describe("describe Update user", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  beforeEach(() => {
    req = {
      body: {
        fullName: "fullName",
        countryCode: 91,
      },
      params: {
        id: "id",
      },
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });
  context("success scenario", () => {
    it("should update the user if valiadtions are successfull", async () => {
      sandbox
        .mock(db)
        .expects("execute")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.instanceOf(UpdateUserQuery);
            expect(args.details).to.be.eql({
              id: req.params.id,
              fullName: req.body.fullName,
              countryCode: req.body.countryCode,
            });
          })
        )
        .returns(resolveOk([1]));
      let response = await TestRoutes.execute("/users/:id", "Put", req, res);
      expect(response).to.be.eql({
        message: "User updated successfully",
        status: true,
        entity: [1],
      });
    });
  });

  it("should not update the user when validations are failed", async () => {
    sandbox
      .stub(UpdateUserValidation, "validate")
      .returns(resolveValidationError("some Error"));
    sandbox.mock(db).expects("execute").never();
    let response = await TestRoutes.executeWithError(
      "/users/:id",
      "Put",
      req,
      res
    );
    expect(response).to.be.eql(new ValidationError(0, "some Error"));
  });

  it("something went wrong unable to update the user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .withArgs(
        verifiers.verifyArgs((args) => {
          expect(args).to.be.instanceOf(UpdateUserQuery);
          expect(args.details).to.be.eql({
            id: req.params.id,
            fullName: req.body.fullName,
            countryCode: req.body.countryCode,
          });
        })
      )
      .returns(resolveError("some random Error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id",
      "Put",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random Error", "something Went wrong")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
