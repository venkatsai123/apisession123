const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const CreateUserValidation = require("resources/users/validation/create-user-validation");

describe("User Validation", async () => {
  context("when correct validations rules are not followed", () => {
    // it("Should mandate fullName as string ", async () => {
    //   const response = await CreateUserValidation.validate({ fullName: 1 });
    //   verifyResultError((error) => {
    //     console.log(error);
    //     expect(error.errorMessage).to.include("fullName should be string");
    //   })(response);
    // });
    it("Should mandate countryCode as integer", async () => {
      const response = await CreateUserValidation.validate({
        countryCode: "91",
      });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include("countryCode should not be string");
      })(response);
    });
    it("Should have length of 2 for countryCode filed", async () => {
      const response = await CreateUserValidation.validate({
        countryCode: 9,
      });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include(
          "countryCode should have length of 2"
        );
      })(response);
    });
    it("Should be correct if all the fields are correctly passed", async () => {
      let response = await CreateUserValidation.validate({
        fullName: "venkat",
        countryCode: 91,
      });
      verifyResultOk(() => {})(response);
    });
  });
});
