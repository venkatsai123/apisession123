const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const CreateRoleValidation = require("resources/roles/validations/create-roles-validation");

describe("Create Role Validation", async () => {
  context("When validation rules are not followed", () => {
    it("should pass to the role name field", async () => {
      let response = await CreateRoleValidation.validate({ name: "" });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include("role name should not be empty");
      })(response);
    });

    it("should have of length of 4  for the role name field", async () => {
      let response = await CreateRoleValidation.validate({ name: "adm" });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include("role name is too short");
      })(response);
    });

    it("should be correct if the role name is passed correctly", async () => {
      let response = await CreateRoleValidation.validate({ name: "admin" });
      verifyResultOk(() => {})(response);
    });
  });
});
