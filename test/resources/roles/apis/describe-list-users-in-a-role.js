const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const db = require("db/repository");
const uuid = require("uuid");
const {
  resolveValidationError,
  resolveOk,
  resolveError,
} = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const ValidationError = require("lib/validation-error");
const verifiers = require("helpers/verifiers");
const ListusersInaRoleQuery = require("resources/roles/query/list-users-in-a-roles-query");

describe("describe List Roles", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  beforeEach(() => {
    req = {
      body: {},
      params: {
        id: "id",
      },
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });
  context("success scenario", () => {
    it("should list all the roles when validations are correct", async () => {
      let returnResult = [
        {
          id: "ae74a53f-8173-4a40-9763-10c8d874863c",
          name: "SDE1",
          createdAt: "2022-07-18T07:40:31.316Z",
          updatedAt: "2022-07-18T07:40:31.316Z",
          UserRoles: {
            roleId: "ae74a53f-8173-4a40-9763-10c8d874863c",
            userId: "535f8ffd-8bf6-46c1-9ee5-a4bc0de52584",
            createdAt: "2022-07-18T07:40:31.326Z",
            updatedAt: "2022-07-18T07:40:31.326Z",
          },
        },
      ];
      sandbox
        .mock(db)
        .expects("execute")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.instanceOf(ListusersInaRoleQuery);
            expect(args.details).to.be.eql({
              id: req.params.id,
            });
          })
        )
        .returns(resolveOk(returnResult));
      let response = await TestRoutes.execute(
        "/users/:id/roles",
        "Get",
        req,
        res
      );
      expect(response).to.be.eql({
        message: "Successfully list the  Roles",
        status: true,
        entity: returnResult,
      });
    });
  });

  it("something went wrong unable to list the roles", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .withArgs(
        verifiers.verifyArgs((args) => {
          expect(args).to.be.instanceOf(ListusersInaRoleQuery);
          expect(args.details).to.be.eql({
            id: req.params.id,
          });
        })
      )
      .returns(resolveError("some random Error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id/roles",
      "Get",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random Error", "Failed to list the Roles")
    );
  });
  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
