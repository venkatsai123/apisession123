const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const db = require("db/repository");
const uuid = require("uuid");
const {
  resolveValidationError,
  resolveOk,
  resolveError,
} = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const ValidationError = require("lib/validation-error");
const MapUserRolesQuery = require("resources/roles/query/map-user-roles-query");
const verifiers = require("helpers/verifiers");

describe("describe create Role", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let id = uuid.v4();
  beforeEach(() => {
    req = {
      body: {},
      params: {
        id: "id",
      },
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  context("Success scenario", () => {
    it("should map user to a role when pass everything", async () => {
      let returnResult = [
        {
          userId: "535f8ffd-8bf6-46c1-9ee5-a4bc0de52584",
          roleId: "ab65ad12-066e-11ed-92a9-494179164c8d",
          createdAt: "2022-07-18T08:31:54.958Z",
          updatedAt: "2022-07-18T08:31:54.958Z",
        },
      ];
      sandbox
        .mock(db)
        .expects("execute")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.instanceOf(MapUserRolesQuery);
            expect(args.details).to.be.eql({
              id: req.params.id,
            });
          })
        )
        .returns(resolveOk(returnResult));
      let response = await TestRoutes.execute(
        "/users/:id/addrole",
        "Post",
        req,
        res
      );
      expect(response).to.be.eql({
        message: "Successfully add the Role to the user",
        status: true,
        entity: returnResult,
      });
    });
  });
  it("something went wrong unable to create the user", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .withArgs(
        verifiers.verifyArgs((args) => {
          expect(args).to.be.instanceOf(MapUserRolesQuery);
          expect(args.details).to.be.eql({
            id: req.params.id,
          });
        })
      )
      .returns(resolveError("some random Error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addrole",
      "Post",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random Error", "Failed to add the Role to the user")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
