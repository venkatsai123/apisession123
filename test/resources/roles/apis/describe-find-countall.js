const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const db = require("db/repository");
const uuid = require("uuid");
const {
  resolveValidationError,
  resolveOk,
  resolveError,
} = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const ValidationError = require("lib/validation-error");
const verifiers = require("helpers/verifiers");
const FindAndCountAllQuery = require("resources/roles/query/find-countall-query");

describe("describe create Role", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  beforeEach(() => {
    req = {
      body: {},
      params: {
        id: "id",
      },
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  context("Success scenario", () => {
    it("Should find and countall when we pass everything correctly ", async () => {
      let returnResult = [
        {
          id: "ae74a53f-8173-4a40-9763-10c8d874863c",
          name: "SDE1",
          createdAt: "2022-07-18T07:40:31.316Z",
          updatedAt: "2022-07-18T07:40:31.316Z",
        },
        {
          id: "ab65ad10-066e-11ed-92a9-494179164c8d",
          name: "user",
          createdAt: "2022-07-18T07:53:11.009Z",
          updatedAt: "2022-07-18T07:53:11.009Z",
        },
        {
          id: "ab65ad11-066e-11ed-92a9-494179164c8d",
          name: "superuser",
          createdAt: "2022-07-18T07:53:11.009Z",
          updatedAt: "2022-07-18T07:53:11.009Z",
        },
        {
          id: "ab65ad12-066e-11ed-92a9-494179164c8d",
          name: "admin",
          createdAt: "2022-07-18T07:53:11.009Z",
          updatedAt: "2022-07-18T07:53:11.009Z",
        },
        {
          id: "ab65ad13-066e-11ed-92a9-494179164c8d",
          name: "enduser",
          createdAt: "2022-07-18T07:53:11.009Z",
          updatedAt: "2022-07-18T07:53:11.009Z",
        },
      ];
      sandbox
        .mock(db)
        .expects("execute")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.instanceOf(FindAndCountAllQuery);
            expect(args.details).to.be.eql({
              id: req.params.id,
            });
          })
        )
        .returns(
          resolveOk({
            count: 5,
            rows: returnResult,
          })
        );
      let response = await TestRoutes.execute(
        "/users/:id/listroles",
        "Get",
        req,
        res
      );
      expect(response).to.be.eql({
        message: "Successfully find and count the Role",
        status: true,
        entity: {
          count: 5,
          rows: returnResult,
        },
      });
    });
  });

  it("something went wrong unable to find and countall the roles", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .withArgs(
        verifiers.verifyArgs((args) => {
          expect(args).to.be.instanceOf(FindAndCountAllQuery);
          expect(args.details).to.be.eql({
            id: req.params.id,
          });
        })
      )
      .returns(resolveError("some random Error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id/listroles",
      "Get",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random Error", "Failed to find and count the Role")
    );
  });
  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
