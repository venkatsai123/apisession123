const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const db = require("db/repository");
const uuid = require("uuid");
const {
  resolveValidationError,
  resolveOk,
  resolveError,
} = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const ValidationError = require("lib/validation-error");
const CreateRoleValidation = require("resources/roles/validations/create-roles-validation");
const CreateRoleQuery = require("resources/roles/query/create-roles-query");
const verifiers = require("helpers/verifiers");

describe("describe create Role", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let userId;
  beforeEach(() => {
    req = {
      body: {
        name: "SDE1",
      },
      params: {
        id: "id",
      },
    };
    (userId = uuid.v4()), sandbox.mock(uuid).expects("v4").returns(userId);
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  context("Success Scenario", () => {
    it("should create role when pass evrything", async () => {
      let returnResult = {
        id: "d18a0d95-7576-4923-b67f-41a3564b3985",
        name: "SDE1",
      };
      sandbox
        .mock(CreateRoleValidation)
        .expects("validate")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.eql({
              name: req.body.name,
            });
          })
        )
        .returns(resolveOk());
      sandbox
        .mock(db)
        .expects("execute")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.instanceOf(CreateRoleQuery);
            expect(args.details).to.be.eql({
              id: userId,
              name: req.body.name,
              userId: req.params.id,
            });
          })
        )
        .returns(resolveOk(returnResult));
      let response = await TestRoutes.execute(
        "/users/:id/roles",
        "Post",
        req,
        res
      );
      expect(response).to.be.eql({
        message: "succussfully able to create role",
        status: true,
        entity: returnResult,
      });
    });
  });
  it("should not create user when validation is failed", async () => {
    sandbox
      .mock(CreateRoleValidation)
      .expects("validate")
      .withArgs(
        verifiers.verifyArgs((args) => {
          expect(args).to.be.eql({
            name: req.body.name,
          });
        })
      )
      .returns(resolveValidationError(["Name of the role is mandatory"]));
    const response = await TestRoutes.executeWithError(
      "/users/:id/roles",
      "Post",
      req,
      res
    );
    expect(response).to.eql(
      new ValidationError(0, ["Name of the role is mandatory"])
    );
  });

  it("something went wrong unable to create the user", async () => {
    sandbox
      .mock(CreateRoleValidation)
      .expects("validate")
      .withArgs(
        verifiers.verifyArgs((args) => {
          expect(args).to.be.eql({
            name: req.body.name,
          });
        })
      )
      .returns(resolveOk());
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random Error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id/roles",
      "Post",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random Error", "something went wrong")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
