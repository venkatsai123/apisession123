const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const CreateAadharValidation = require("resources/aadhar/validation/create-aadhar-validation");

describe("Creating aadhar for the user", async () => {
  context("when correct rules are not followed for validation", () => {
    it("should pass aadharNumber to the aadharField", async () => {
      let response = await CreateAadharValidation.validate({
        aadharNumber: "",
      });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include(
          "Aadhar number should not be empty"
        );
      })(response);
    });
    it("should pass string to the aadharnumber filed", async () => {
      let response = await CreateAadharValidation.validate({
        aadharNumber: 123456789112,
      });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include("Aadhar number should be string");
      })(response);
    });
    it("should have length of 12", async () => {
      let response = await CreateAadharValidation.validate({
        aadharNumber: "12345678911",
      });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include(
          "aadhar number should have 12 numbers"
        );
      })(response);
    });
    it("Should be correct if all the fields are correctly passed", async () => {
      let response = await CreateAadharValidation.validate({
        aadharNumber: "123456789112",
      });
      verifyResultOk(() => {})(response);
    });
  });
});
