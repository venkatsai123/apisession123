const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveValidationError,
  resolveOk,
  resolveError,
} = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const ValidationError = require("lib/validation-error");
const CreateAadharValidation = require("resources/aadhar/validation/create-aadhar-validation");
const verifiers = require("helpers/verifiers");
const CreateAadharQuery = require("resources/aadhar/query/create-aadhar-query");

describe("describe create aadhar for particular user", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let userId;
  beforeEach(() => {
    req = {
      body: {
        aadharNumber: "281632996556",
      },
      params: {
        userId,
      },
    };
    (userId = uuid.v4()), sandbox.mock(uuid).expects("v4").returns(userId);
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  context("Success Senario", () => {
    it("should create aadhar if validations are successful", async () => {
      let returnResult = {
        id: "userId",
        aadharNumber: "281632996556",
      };
      sandbox
        .mock(CreateAadharValidation)
        .expects("validate")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.eql({
              aadharNumber: req.body.aadharNumber,
            });
          })
        )
        .returns(resolveOk(returnResult));
      sandbox
        .mock(db)
        .expects("execute")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.instanceOf(CreateAadharQuery);
            expect(args.details).to.be.eql({
              id: userId,
              aadharNumber: req.body.aadharNumber,
              userId: req.params.userId,
            });
          })
        )
        .returns(resolveOk(returnResult));
      const response = await TestRoutes.execute(
        "/users/:userId/aadhar",
        "Post",
        req,
        res
      );
      console.log(response);
      expect(response).to.be.eql({
        message: "Aadhar Created Successfully",
        status: true,
        entity: returnResult,
      });
    });
  });

  it("should not create aadhar when validation is failed", async () => {
    sandbox
      .stub(CreateAadharValidation, "validate")
      .returns(resolveValidationError("AadharNumber is mandatory"));
    sandbox.mock(db).expects("execute").never()
    const response = await TestRoutes.executeWithError(
      "/users/:userId/aadhar",
      "Post",
      req,
      res
    );
    expect(response).to.eql(
      new ValidationError(0, "AadharNumber is mandatory")
    );
  });

  it("something went wrong unable to create the Aadhar", async () => {
    sandbox
      .mock(CreateAadharValidation)
      .expects("validate")
      .returns(resolveOk());
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random Error"));
    const response = await TestRoutes.executeWithError(
      "/users/:userId/aadhar",
      "Post",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random Error", "something Went wrong")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
