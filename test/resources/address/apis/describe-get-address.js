const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveValidationError,
  resolveOk,
  resolveError,
} = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const ValidationError = require("lib/validation-error");

describe("describe get Addresses", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  beforeEach(() => {
    req = {
      body: {},
      params: {
        userId: 'userId'
      },
    };
    
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });
  context("Success senario", () => {
    it("should get addresses when we pass everything", async () => {
      let returnResult = [
        {
          id: "219d8b3b-a81e-4f94-9675-ea04d896129c",
          name: "venkat sai",
          street: "Hanuman street",
          city: "Bihar",
          country: "India",
          userId: "c7e1ed70-fd0c-11ec-8855-5f9e03fceddc",
          createdAt: "2022-07-12T08:12:19.756Z",
          updatedAt: "2022-07-12T08:12:19.756Z",
        },
      ];
      sandbox.mock(db).expects("execute").returns(resolveOk(returnResult));
      const response = await TestRoutes.execute(
        "/users/:userId/addresses",
        "Get",
        req,
        res
      );
      expect(response).to.be.eql({
        message: "Address Created Successfully",
        status: true,
        entity: returnResult,
      });
    });
  });

  it("something went wrong unable to get the address", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random Error"));
    const response = await TestRoutes.executeWithError(
      "/users/:userId/addresses",
      "Get",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random Error", "something Went wrong")
    );
  });
  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
