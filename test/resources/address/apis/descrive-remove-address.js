const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveValidationError,
  resolveOk,
  resolveError,
} = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const ValidationError = require("lib/validation-error");
const verifiers = require("helpers/verifiers");
const RemoveAddressQuery = require("resources/address/query/remove-address-query");

describe("describe get Addresses", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let id = uuid.v4();
  beforeEach(() => {
    req = {
      body: {},
      params: {
        id: "id",
        addId: "id",
      },
    };
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  context("Success Scenario", () => {
    it("should delete the address when all validations are passed", async () => {
      sandbox
        .mock(db)
        .expects("execute")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.instanceOf(RemoveAddressQuery);
            expect(args.details).to.be.eql({
              userId: req.params.id,
              addressId: req.params.addId,
            });
          })
        )
        .returns(resolveOk([]));
      let response = await TestRoutes.execute(
        "/users/:id/addresses/:addId",
        "Delete",
        req,
        res
      );
      expect(response).to.be.eql({
        message: "Address Removed Successfully",
        status: true,
        entity: [],
      });
    });
  });

  it("something went wrong unable to delete the address", async () => {
    sandbox
      .mock(db)
      .expects("execute")
      .withArgs(
        verifiers.verifyArgs((args) => {
          expect(args).to.be.instanceOf(RemoveAddressQuery);
          expect(args.details).to.be.eql({
            userId: req.params.id,
            addressId: req.params.addId,
          });
        })
      )
      .returns(resolveError("some random Error"));
    const response = await TestRoutes.executeWithError(
      "/users/:id/addresses/:addId",
      "Delete",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random Error", "something Went wrong")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
