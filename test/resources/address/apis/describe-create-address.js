const ApiError = require("lib/functional/api-error");
const sinon = require("sinon");
const sinonChai = require("sinon-chai");
const chai = require("chai");
chai.use(sinonChai);
const expect = chai.expect;
const uuid = require("uuid");
const db = require("db/repository");
const {
  resolveValidationError,
  resolveOk,
  resolveError,
} = require("helpers/resolvers");
const TestRoutes = require("helpers/test-route");
const ValidationError = require("lib/validation-error");
const CreateAddressValidation = require("resources/address/validations/create-address-validation");
const verifiers = require("helpers/verifiers");
const CreateAddressQuery = require("resources/address/query/create-address-query");

describe("describe create Addresses", () => {
  let sandbox = sinon.createSandbox();
  let req, res;
  let userId;
  beforeEach(() => {
    req = {
      body: {
        name: "name",
        street: "street",
        city: "city",
        country: "country",
      },
      params: {
        userId: "userId",
      },
    };
    (userId = uuid.v4()), sandbox.mock(uuid).expects("v4").returns(userId);
    res = {
      setHeader: sandbox.spy(),
      send: sandbox.spy(),
      status: sandbox.spy(() => {
        return res;
      }),
    };
  });

  context("Success Senario", () => {
    it("should create address when we pass everything", async () => {
      let returnResult = {
        id: "userId",
        name: "venkat sai",
        street: "Hanuman street",
        city: "Bihar",
        country: "India",
        userId: "c7e1ed70-fd0c-11ec-8855-5f9e03fceddc",
      };
      sandbox
        .mock(CreateAddressValidation)
        .expects("validate")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.eql({
              name: req.body.name,
              street: req.body.street,
              city: req.body.city,
              country: req.body.country,
            });
          })
        )
        .returns(resolveOk());
      sandbox
        .mock(db)
        .expects("execute")
        .withArgs(
          verifiers.verifyArgs((args) => {
            expect(args).to.be.instanceOf(CreateAddressQuery);
            expect(args.details).to.be.eql({
              id: userId,
              name: req.body.name,
              street: req.body.street,
              city: req.body.city,
              country: req.body.country,
              userId: req.params.userId,
            });
          })
        )
        .returns(resolveOk(returnResult));
      const response = await TestRoutes.execute(
        "/users/:userId/addresses",
        "Post",
        req,
        res
      );
      expect(response).to.be.eql({
        message: "Address Created Successfully",
        status: true,
        entity: returnResult,
      });
    });
  });

  it("should not create address when validation is failed", async () => {
    sandbox
      .stub(CreateAddressValidation, "validate")
      .returns(resolveValidationError("some error"));
    sandbox.mock(db).expects("execute").never();
    const response = await TestRoutes.executeWithError(
      "/users/:userId/addresses",
      "Post",
      req,
      res
    );
    expect(response).to.eql(new ValidationError(0, "some error"));
  });

  it("something went wrong unable to create the address", async () => {
    sandbox
      .mock(CreateAddressValidation)
      .expects("validate")
      .withArgs(
        verifiers.verifyArgs((args) => {
          expect(args).to.be.eql({
            name: req.body.name,
            street: req.body.street,
            city: req.body.city,
            country: req.body.country,
          });
        })
      )
      .returns(resolveOk());
    sandbox
      .mock(db)
      .expects("execute")
      .returns(resolveError("some random Error"));
    const response = await TestRoutes.executeWithError(
      "/users/:userId/addresses",
      "Post",
      req,
      res
    );
    expect(response).to.be.eql(
      new ApiError(0, "some random Error", "something Went wrong")
    );
  });

  afterEach(() => {
    sandbox.verifyAndRestore();
  });
});
