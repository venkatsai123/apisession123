const chai = require("chai");
const expect = chai.expect;
const { verifyResultOk, verifyResultError } = require("helpers/verifiers");
const CreateAddressValidation = require("resources/address/validations/create-address-validation");

describe("Create Address Validation", async () => {

  context("when Correct validations rules are not followed", () => {

    it("should pass to the name filed", async () => {

      let response = await CreateAddressValidation.validate({ name: "" });

      verifyResultError((error) => {
        expect(error.errorMessage).to.include("name shouldnot be empty");
      })(response);

    });

    it("should have length of atleast 4 for the name filed", async () => {
      let response = await CreateAddressValidation.validate({ name: "ven" });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include(
          "name should not be less than length of 4"
        );
      })(response);

    });

    it("should pass to the street filed", async () => {
      let response = await CreateAddressValidation.validate({ street: "" });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include("street shouldnot be empty");
      })(response);

    });

    it("should have length of atleast 4 for the street filed", async () => {
      let response = await CreateAddressValidation.validate({ street: "hsr" });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include(
          "street should not be less than length of 4"
        );
      })(response);
    });
    it("should pass to the city filed", async () => {
      let response = await CreateAddressValidation.validate({ city: "" });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include("city shouldnot be empty");
      })(response);
    });
    it("should have length of atleast 4 for the city filed", async () => {
      let response = await CreateAddressValidation.validate({ city: "bgl" });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include(
          "city should not be less than length of 4"
        );
      })(response);
    });
    it("should pass to the country filed", async () => {
      let response = await CreateAddressValidation.validate({ country: "" });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include("country shouldnot be empty");
      })(response);
    });
    it("should have length of atleast 4 for the city filed", async () => {
      let response = await CreateAddressValidation.validate({ country: "Ind" });
      verifyResultError((error) => {
        expect(error.errorMessage).to.include(
          "country should not have shortcutname"
        );
      })(response);

    });
    
    it("should be correct if all fileds are correctly passed", async () => {
      let response = await CreateAddressValidation.validate({
        name: "venkat",
        street: "Hanuman street",
        city: "Bnglr",
        country: "India",
      });
      verifyResultOk(() => {})(response);
    });
  });
});
