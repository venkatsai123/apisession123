const Route = require("route");
const db = require("db/repository");
const GetAddressQuery = require("resources/address/query/get-addresses-query");
const { respond, composeResult } = require("lib");
const { logInfo } = require("lib/functional/logger");

async function get(req) {
  const userId = req.params.userId;
  logInfo("Request for getting the address");
  let response = await composeResult(() => {
    return db.execute(new GetAddressQuery(userId));
  })();
  return respond(
    response,
    "Address Created Successfully",
    "something Went wrong"
  );
}

Route.withOutSecurity().noAuth().get("/users/:userId/addresses", get).bind();

module.exports.get = get;

