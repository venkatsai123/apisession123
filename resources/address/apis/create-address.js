const Route = require("route");
const db = require("db/repository");
const uuid = require("uuid");
const CreateAddressQuery = require("resources/address/query/create-address-query");
const { respond, whenResult } = require("lib");
const { logInfo } = require("lib/functional/logger");
const createAddressValidation = require("resources/address/validations/create-address-validation");

async function post(req, res) {
  const userId = req.params.userId;
  const { name, street, city, country } = req.body;
  logInfo("Request for creating address", { name, street, city, country });
  const validation = await createAddressValidation.validate({
    name,
    street,
    city,
    country,
  });
  const id = uuid.v4();
  const response = await whenResult(() => {
    return db.execute(
      new CreateAddressQuery({
        id,
        name,
        street,
        city,
        country,
        userId,
      })
    );
  })(validation);
  return respond(
    response,
    "Address Created Successfully",
    "something Went wrong"
  );
}

Route.withOutSecurity().noAuth().post("/users/:userId/addresses", post).bind();

module.exports.post = post;

