const Route = require("route");
const db = require("db/repository");
const uuid = require("uuid");
const RemoveAddressQuery = require("resources/address/query/remove-address-query");
const { respond,composeResult } = require("lib");
const { logInfo } = require("lib/functional/logger");

async function remove(req) {
  const userId = req.params.id;
  const addressId = req.params.addId;
  logInfo("Request for removal of address")
  const response = await composeResult(() => {
    return db.execute(new RemoveAddressQuery(userId, addressId));
  })();
  return respond(
    response,
    "Address Removed Successfully",
    "something Went wrong"
  );
}

Route.withOutSecurity()
  .noAuth()
  .delete("/users/:id/addresses/:addId", remove)
  .bind();

module.exports.delete = remove;

