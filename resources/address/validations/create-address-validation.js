const { notEmpty, validate, minValue } = require("validation");
const R = require("ramda");
const rule = {
  name: [
    [notEmpty, "name shouldnot be empty"],
    [
      (value, obj) => {
        return minValue(4, R.length(value));
      },
      "name should not be less than length of 4",
    ],
  ],
  street: [
    [notEmpty, "street shouldnot be empty"],
    [
      (value, obj) => {

        return minValue(4, R.length(value));
      },
      "street should not be less than length of 4",
    ],
  ],
  city: [
    [notEmpty, "city shouldnot be empty"],
    [
      (value, obj) => {
        // console.log(minValue(4, value));

        return minValue(4, R.length(value));
      },
      "city should not be less than length of 4",
    ],
  ],
  country: [
    [notEmpty, "country shouldnot be empty"],
    [
      (value, obj) => {
        // console.log(minValue(4, value));

        return minValue(4, R.length(value));
      },
      "country should not have shortcutname",
    ],
  ],
};
module.exports.validate = async (data) => validate(rule, data);
