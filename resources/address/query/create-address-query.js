const Models = require('../../../models');

module.exports = class CreateAddressQuery{
    constructor(details){
        this.details =details
    }
    async get() {
        console.log(this.details);
        let user = await Models.User.findOne({
            where:{
            id:this.details.userId}
        });

        // let address = await Models.Addresses.create({name:this.details.name,street:this.details.street,city:this.details.city,country:this.details.country});
        return user.createAddress({name:this.details.name,street:this.details.street,city:this.details.city,country:this.details.country});
        // return user.addAddresses(address);
    }
    
}