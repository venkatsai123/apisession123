const db = require("db/repository");
const { logInfo } = require("lib/functional/logger");
const CreateRolesQuery = require("resources/roles/query/create-roles-query");
const uuid = require("uuid");
const { respond, composeResult, withArgs } = require("lib");
const Route = require("route");
const Models = require("models");
const createRolesValidate = require("resources/roles/validations/create-roles-validation");

async function post(req, res) {
  
  const userId = req.params.id;

  const { name } = req.body;

  logInfo("Request for creating the role", {});

  const response = await composeResult(
    withArgs(db.execute, new CreateRolesQuery({ id: uuid.v4(), name, userId })),
    createRolesValidate.validate
  )({ name });

  return respond(
    response,
    "succussfully able to create role",
    "something went wrong"
  );
}

Route.withOutSecurity().noAuth().post("/users/:id/roles", post).bind();
module.exports.post = post;
