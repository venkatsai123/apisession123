const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond, composeResult } = require("lib");
const db = require("db/repository");
const uuid = require("uuid");
const Models = require("models");
const MapUserRoleQuery = require("resources/roles/query/map-user-roles-query");

async function post(req, res) {
  const id = req.params.id;

  logInfo("Request to list role in a user", {});
  const response = await db.execute(new MapUserRoleQuery(id));
  return respond(
    response,
    "Successfully add the Role to the user",
    "Failed to add the Role to the user"
  );
}

Route.withOutSecurity().noAuth().post("/users/:id/addrole", post).bind();
module.exports.post = post;

