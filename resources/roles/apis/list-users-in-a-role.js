const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond } = require("lib");
const db = require("db/repository");
const uuid = require("uuid");
const Models = require("models");
const ListUserInARoleQuery = require("resources/roles/query/list-users-in-a-roles-query");
async function post(req, res) {
  const id = req.params.id;

  logInfo("Request to list role in a user", {});
  const response = await db.execute(new ListUserInARoleQuery(id));
  return respond(
    response,
    "Successfully list the  Roles",
    "Failed to list the Roles"
  );
}

Route.withOutSecurity().noAuth().get("/users/:id/roles", post).bind();
module.exports.post = post;
