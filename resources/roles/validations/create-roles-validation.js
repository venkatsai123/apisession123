const { validate, notEmpty, minValue } = require("validation");
const R = require("ramda");
const rules = {
  name: [
    [notEmpty, "role name should not be empty"],
    [
      (value, obj) => {
        return minValue(4, R.length(value));
      },
      "role name is too short"
    ],
  ],
};

module.exports.validate = async (data) => validate(rules, data);

