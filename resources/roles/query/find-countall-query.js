const Models = require("models");

module.exports = class Pagination {
  constructor(id) {
    this.details = {
      id,
    };
  }
  async get() {
    return await Models.Roles.findAndCountAll({
      limit: 2,
      order: ["ASC"],
    });
  }
};

