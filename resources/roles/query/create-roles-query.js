const Models = require("models");

module.exports = class CreateRolesQuery {
  constructor({id, name, userId}) {
    this.details = {
      id,
      name,
      userId,
    };
  }

  async get() {
    const user = await Models.User.findOne({
      where: {
        id: this.details.userId,
      },
    });
    return user.createRole({
      id: this.details.id,
      name: this.details.name,
    });
  }
};

