const Models = require("models");

module.exports = class MapUserRoleQuery {
  constructor(id) {
    this.details = {
      id,
    };
  }
  async get() {
    const roles = await Models.Roles.findOne({
      where: {
        id: "e7c1da7f-7217-409d-888f-6ed4636d30af",
      },
    });
    const users = await Models.User.findOne({
      where: {
        id: this.details.id,
      },
    });

    return await users.addRole(roles);
  }
};

