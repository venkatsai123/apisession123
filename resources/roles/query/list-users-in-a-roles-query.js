const Models = require("models");

module.exports = class ListUserInARoleQuery {
  constructor(id) {
    this.details = {
      id,
    };
  }
  async get() {
    const user = await Models.User.findOne({
      where: {
        id: this.details.id
      }
    })
    return user.getRoles();
  }
};


