const Route = require("route");
const db = require("db/repository");
const uuid = require("uuid");
const CreateAadharQuery = require("resources/aadhar/query/create-aadhar-query");
const { respond, composeResult, withArgs } = require("lib");
const { logInfo } = require("lib/functional/logger");
const validateCreateAadhar = require("resources/aadhar/validation/create-aadhar-validation");

async function post(req, res) {
  const userId = req.params.userId;
  const { aadharNumber } = req.body;
  logInfo("Request to create aadhar", {aadharNumber});

  // const validation = await validateCreateAadhar.validate({ aadharNumber });
  const response = await composeResult(
    withArgs(
      db.execute,
      new CreateAadharQuery({ id: uuid.v4(), aadharNumber, userId })
    ),
    validateCreateAadhar.validate
  )({ aadharNumber });

  return respond(
    response,
    "Aadhar Created Successfully",
    "something Went wrong"
  );
}

Route.withOutSecurity().noAuth().post("/users/:userId/aadhar", post).bind();

module.exports.post = post;
