const { validate, notEmpty, hasLengthOf,string } = require("validation");

const rule = {
  aadharNumber: [
    [notEmpty, "Aadhar number should not be empty"],
    [string, "Aadhar number should be string"],
    [
      (value, obj) => {
        return hasLengthOf(12, value);
      },
      "aadhar number should have 12 numbers",
    ],
  ],
};

module.exports.validate = async (data) => validate(rule, data);
