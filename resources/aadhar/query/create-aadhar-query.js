const Models = require("../../../models");

module.exports = class CreateAadharQuery {
  constructor(details) {
    this.details = details;
  }

  async get() {
    let user = await Models.User.findOne({
      where: {
        id: this.details.userId,
      },
    });

    return user.createAadhar({
      name: this.details.name,
      aadharNumber: this.details.aadharNumber,
    });
  }
};
