const Route = require("route");
const db = require("db/repository");
const UpdateUserQuery = require("resources/users/query/update-user-query");
const { respond, composeResult, withArgs } = require("../../../lib");
const { logInfo } = require("lib/functional/logger");
const updateUserValidation = require("resources/users/validation/update-user-validation");

async function put(req) {
  const { fullName, countryCode } = req.body;

  logInfo("Request for Updating the data", { fullName, countryCode });

  let userId = req.params.id;

  let response = await composeResult(
    withArgs(db.execute, new UpdateUserQuery(userId, fullName, countryCode)),
    updateUserValidation.validate
  )({ fullName, countryCode });

  return respond(response, "User updated successfully", "something Went wrong");
}

Route.withOutSecurity().noAuth().put("/users/:id", put).bind();

module.exports.put = put;
