const Route = require("route");
const db = require("db/repository");
const GetUsersQuery = require("resources/users/query/get-users-query");
const { respond, composeResult } = require("lib");
const { logInfo } = require("lib/functional/logger");

async function get() {
  logInfo("Request to get all users", {});
  const response = await composeResult(() => {
    return db.execute(new GetUsersQuery());
  })();
  return respond(response, "Fetched users succesfully", "something Went wrong");
}

Route.withOutSecurity().noAuth().get("/users", get).bind();

module.exports.get = get;
