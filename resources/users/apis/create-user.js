const Route = require("route");
const { logInfo } = require("lib/functional/logger");
const { respond, composeResult, withArgs } = require("lib");
const Result = require("folktale/result");
const db = require("db/repository");
const uuid = require("uuid");
const CreateUserQuery = require("resources/users/query/create-user-query");
const createUserValidation = require("resources/users/validation/create-user-validation");

async function post(req, res) {
  const { fullName, countryCode } = req.body;

  logInfo("Request to create user", {
    fullName,
    countryCode,
  });

  const id = uuid.v4();

  const response = await composeResult(
    ()=>db.execute (new CreateUserQuery(id, fullName,countryCode)),
    createUserValidation.validate
  )({ fullName, countryCode });

  return respond(
    response,
    "Successfully created user",
    "Failed to create user"
  );
}

Route.withOutSecurity().noAuth().post("/users", post).bind();
module.exports.post = post;
