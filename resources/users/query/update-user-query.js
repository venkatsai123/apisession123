const Models = require("../../../models");

module.exports = class UpdateUserQuery {
  constructor(id,fullName,countryCode) {
    this.details = {id,fullName,countryCode};
  }

  get() {
    console.log(this.details) 
    return Models.User.update(
      {
        fullName: this.details.fullName,
        countryCode: this.details.countryCode,
      },
      {
        where: {
          id: this.details.id,
        },
        returning:true
      }
    );
  }
};
