const { validate, notEmpty, hasLengthOf } = require("validation");

const rule = {
  fullName: [[notEmpty, "Full Name should not be empty!"]],
  countryCode: [
    [notEmpty, "countryCode Should not be empty!"],
    [
      (value, obj) => {
        return hasLengthOf(2, JSON.stringify(value));
      },
      "countryCode should be length of 2",
    ],
  ],
};

module.exports.validate = async (data) => validate(rule, data);
