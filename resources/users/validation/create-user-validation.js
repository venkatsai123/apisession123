const { validate, notEmpty, hasLengthOf,numeric } = require("validation");

const rule = {
  fullName: [[notEmpty, "Full Name should not be empty!"]],
  countryCode: [
    [notEmpty, "countryCode Should not be empty!"],
    [numeric,"countryCode should not be string"],
    [
      (value, obj) => {
        return hasLengthOf(2, JSON.stringify(value));
      },
      "countryCode should have length of 2",
    ],
  ],
};

module.exports.validate = async (data) => validate(rule, data);
